import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import DefaultExample from './pages/00_default'
import UseRefExample from './pages/01_useRef'
import FowardRefExample from './pages/02_fowardRef'
import UseCallbackExample from './pages/03_useCallback'
import UseCallbackAndUseRefExample from './pages/04_useCallback_useRef'
import UseMemoAndUseRefExample from './pages/05_useMemo_useRef'
import ReactMemoExample from './pages/06_react_memo'
import ReactDeepMemoExample from './pages/07_react_deepmemo'

const Routes = () => (
  <Router>
    <Switch>
      <Route path="/00">
        <DefaultExample />
      </Route>
      <Route path="/01">
        <UseRefExample />
      </Route>
      <Route path="/02">
        <FowardRefExample />
      </Route>
      <Route path="/03">
        <UseCallbackExample />
      </Route>
      <Route path="/04">
        <UseCallbackAndUseRefExample />
      </Route>
      <Route path="/05">
        <UseMemoAndUseRefExample />
      </Route>
      <Route path="/06">
        <ReactMemoExample />
      </Route>
      <Route path="/07">
        <ReactDeepMemoExample />
      </Route> 
    </Switch>
  </Router>
)

export default Routes