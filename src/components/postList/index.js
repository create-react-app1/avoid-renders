import PostItem from '../postItem'

const PostList = ({ posts = [] }) => (
  <div>{ posts.map(p => <PostItem key={p.id} post={p} />)}</div>
)

export default PostList