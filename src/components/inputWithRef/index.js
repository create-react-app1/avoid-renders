import React from 'react'

const InputWithRef = React.forwardRef((props, ref) => 
  <input ref={ref} {...props} /> 
)

export default InputWithRef