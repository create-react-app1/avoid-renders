import React from 'react'
import PostItemMemo from '../postItemMemo'

const postListDeepMemo = ({ posts = [] }) => (
  <div>{ posts.map(p => <PostItemMemo key={p.id} post={p} />)}</div>
)

export default React.memo(postListDeepMemo, 
  (prevProps, nextProps) => JSON.stringify(prevProps) === JSON.stringify(nextProps)
)