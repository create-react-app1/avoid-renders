import React from "react";

const PostItemMemo = ({ post }) => (
  <article>
    <strong>{post.title}</strong>
    <p>{post.body}</p>
  </article>
)

export default React.memo(PostItemMemo)