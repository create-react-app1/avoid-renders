import React from 'react'
import PostItemMemo from '../postItemMemo'

const PostListMemo = ({ posts = [] }) => (
  <div>{ posts.map(p => <PostItemMemo key={p.id} post={p} />)}</div>
)

export default React.memo(PostListMemo)