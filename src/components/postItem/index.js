const PostItem = ({ post }) => (
  <article>
    <strong>{post.title}</strong>
    <p>{post.body}</p>
  </article>
)

export default PostItem