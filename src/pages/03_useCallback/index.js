import React, { useState, useEffect, useCallback } from 'react'
import { getPosts } from '../../api'

import PostList from '../../components/postList'

const UseCallbackExample = () => {
  const [posts, setPosts] = useState([])
  const [filterdPosts, setFilteredPosts] = useState()
  const [input, setInput] = useState([])

  useEffect(() => {
    getPosts().then(data => setPosts(data))
  }, [])

  const handleFilter = () => {
    const getFilteredPosts = input.length > 3 ? posts.filter(p => {
      const completePost = [p.title, p.body].join(" ")
      return completePost.match(input)
    }) : posts
    setFilteredPosts(getFilteredPosts)
  }

  /*
    Obs: a função Posts será reconstruida apenas quando houver modificação nos 'posts' ou no
    'fiteredPosts', porém isso não evita a renderização dele pois pois a cada atualização deste
    componente, sua função é chamada no 'return'.
  */
  const Posts = useCallback(() => (
    <PostList posts={filterdPosts || posts} />
  ), [JSON.stringify(posts), JSON.stringify(filterdPosts)])
  // const Posts = () => <PostList posts={filterdPosts || posts} />

  useEffect(() => console.log("mudei o Posts"), [Posts])

  return (
    <div>
      <div>
        <input onChange={(e) => setInput(e.target.value)} onBlur={handleFilter} />
      </div>
      
      {Posts()}
    </div>
  )

}

export default UseCallbackExample