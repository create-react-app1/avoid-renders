import React, { useState, useEffect, createRef } from 'react'
import { getPosts } from '../../api'

import PostListMemo from '../../components/postListMemo'
import InputWithRef from '../../components/inputWithRef'

const ReactMemoExample = () => {
  const [posts, setPosts] = useState([])
  const [filterdPosts, setFilteredPosts] = useState()
  const inputRef = createRef()

  useEffect(() => {
    getPosts().then(data => setPosts(data))
  }, [])

  const handleFilter = () => {
    const input = inputRef.current.value
    const getFilteredPosts = input.length > 3 ? posts.filter(p => {
      const completePost = [p.title, p.body].join(" ")
      return completePost.match(input)
    }) : posts
    setFilteredPosts(getFilteredPosts)
  }

  return (
    <div>
      <div>
        <InputWithRef ref={inputRef} onBlur={handleFilter} />
      </div>
      
      <PostListMemo posts={filterdPosts || posts} />
    </div>
  )

}

export default ReactMemoExample