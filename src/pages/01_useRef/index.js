import React, { useState, useEffect, createRef } from 'react'
import { getPosts } from '../../api'

import PostList from '../../components/postList'

const UseRefExample = () => {
  const [posts, setPosts] = useState([])
  const inputRef = createRef()

  useEffect(() => {
    getPosts().then(data => setPosts(data))
  }, [])

  const handleClick = () => alert(inputRef.current.value)

  return (
    <div>
      <div>
        <input ref={inputRef} />
        <button onClick={handleClick}>Send</button>
      </div>
      
      <PostList posts={posts} />
    </div>
  )

}

export default UseRefExample