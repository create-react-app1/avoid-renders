import React, { useState, useEffect, createRef, useCallback } from 'react'
import { getPosts } from '../../api'

import PostList from '../../components/postList'
import InputWithRef from '../../components/inputWithRef'

const UseRefExample = () => {
  const [posts, setPosts] = useState([])
  const inputRef = createRef()

  useEffect(() => {
    getPosts().then(data => setPosts(data))
  }, [])

  const handleClick = () => alert(inputRef.current.value)

  return (
    <div>
      <div>
        <InputWithRef ref={inputRef} />
        <button onClick={handleClick}>Send</button>
      </div>
      
      <PostList posts={posts} />
    </div>
  )

}

export default UseRefExample