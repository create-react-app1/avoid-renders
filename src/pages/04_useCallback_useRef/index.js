
import React, { useState, useEffect, createRef, useCallback } from 'react'
import { getPosts } from '../../api'

import PostList from '../../components/postList'
import InputWithRef from '../../components/inputWithRef'

const UseCallbackAndUseRefExample = () => {
  const [posts, setPosts] = useState([])
  const [filterdPosts, setFilteredPosts] = useState()
  const inputRef = createRef()

  useEffect(() => {
    getPosts().then(data => setPosts(data))
  }, [])

  const handleFilter = () => {
    const input = inputRef.current.value
    const getFilteredPosts = input.length > 3 ? posts.filter(p => {
      const completePost = [p.title, p.body].join(" ")
      return completePost.match(input)
    }) : posts
    setFilteredPosts(getFilteredPosts)
  }

  const Posts = useCallback(() => (
    <PostList posts={filterdPosts || posts} />
  ), [JSON.stringify(posts), JSON.stringify(filterdPosts)])

  return (
    <div>
      <div>
        <InputWithRef ref={inputRef} onBlur={handleFilter} />
      </div>
      
      {Posts()}
    </div>
  )

}

export default UseCallbackAndUseRefExample