import React, { useState, useEffect } from 'react'
import { getPosts } from '../../api'

import PostList from '../../components/postList'

const DefaultExample = () => {
  const [posts, setPosts] = useState([])
  const [input, setInput] = useState("")

  useEffect(() => {
    getPosts().then(data => setPosts(data))
  }, [])

  return (
    <div>
      <div>
        <input onChange={(e) => setInput(e.target.value)} />
      </div>
      
      <PostList posts={posts} />
    </div>
  )

}

export default DefaultExample