import mock from './mock.json'

export const getPosts = async () => 
  await fetch('https://jsonplaceholder.typicode.com/posts').then(response => 
    response.json()
    .then( data => data )
    .catch(() => mock)
)

